package at.hakwt.swp4.coupling;

public class CouplingMain {

    public static void main(String[] args)
    {
        Vehicle v1 = new Car();
	    Traveler t = new Traveler(v1);
	    t.startJourney();
    }
}
