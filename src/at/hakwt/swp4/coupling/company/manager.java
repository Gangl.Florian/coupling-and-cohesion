package at.hakwt.swp4.coupling.company;

public class manager
{

    private  employees[] employees;
    public manager(employees[] e)
    {
        this.employees = e;
    }

    public void manageWork() {

        for (employees  e : employees) {

            e.process(new String[]{"this", "and", "time"});
        }
    }

}
