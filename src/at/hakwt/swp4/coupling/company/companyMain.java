package at.hakwt.swp4.coupling.company;

public class companyMain
{
    public static void main(String[] args) {

        worker panyF = new worker();
        worker gerli = new worker();

        manager robert = new manager(new worker[]{panyF, gerli});
        robert.manageWork();

        Accountant Pia = new Accountant();
        Accountant Florian = new Accountant();
        manager Lucas = new manager(new Accountant[]{Pia,Florian});
        Lucas.manageWork();
    }

}
