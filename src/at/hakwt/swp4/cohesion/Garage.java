package at.hakwt.swp4.cohesion;

public class Garage
{

    public static void parts()
    {
        System.out.println("The Garage has to buy the parts for the car");
    }


    public static void work()
    {
        System.out.println("The Garage has to work on the car");
    }

}
