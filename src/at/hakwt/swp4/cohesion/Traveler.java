package at.hakwt.swp4.cohesion;

public class Traveler {

    public void startJourney() {
        // ...
    }

    public void maintainCar()
    {

        Garage.work();
        Garage.parts();
        System.out.println("Bring the car to the Restaurante");
        // put car into garage
        // buy maintainacne parts
        // do maintainance work
    }

    public void cook()
    {
        Restaurante.cook();
        Restaurante.ingredients();
        System.out.println("Find the right Restaurante");
        // find place to do cooking
        // buy ingredients
        // do the cooking
    }


}
