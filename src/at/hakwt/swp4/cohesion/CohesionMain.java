package at.hakwt.swp4.cohesion;

public class CohesionMain {

    public static void main(String[] args)
    {
        
        Traveler Florian = new Traveler();
        Florian.cook();
        Florian.maintainCar();

        Garage Dobersberg = new Garage();
        Dobersberg.parts();
        Dobersberg.work();

        Restaurante Meli = new Restaurante();
        Meli.ingredients();
        Meli.cook();
    }
}
