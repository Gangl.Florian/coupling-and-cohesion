package at.hakwt.swp4.cohesion.payroll;

public class Expense
{
private double value;

    public Expense(double value)
    {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
