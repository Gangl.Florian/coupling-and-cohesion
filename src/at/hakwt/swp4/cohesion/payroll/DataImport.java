package at.hakwt.swp4.cohesion.payroll;

public class DataImport
{
    public PayrollData importData (String fileName)
    {
        Expense diaeten = new Expense(53.8);
        Expense kmGeld = new Expense(100 + 0.42);
        Employee max =new Employee(1500, "Max Mustermann");
        return new PayrollData(max, new Expense[]{diaeten, kmGeld}, 3);
    }
}
