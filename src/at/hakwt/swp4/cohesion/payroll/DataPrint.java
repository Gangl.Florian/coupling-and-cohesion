package at.hakwt.swp4.cohesion.payroll;

public class DataPrint {

    public void print(Employee employee, double amount, String month)
    {
        amount= Math.round((amount * 100.0)/100.0);
        System.out.println("Mitarbeiter " + employee.getName()+ " bekommt im Monat " + month + " "+ amount + "€ ausbezahlt");
    }
}
