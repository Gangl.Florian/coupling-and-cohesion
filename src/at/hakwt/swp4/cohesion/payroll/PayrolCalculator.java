package at.hakwt.swp4.cohesion.payroll;

import java.util.Random;

public class PayrolCalculator
{

    public double calculatePayment(Employee employee, int noOfhours, Expense[] expenses)
    {
        double total = employee.getSalary();

        //calculate overhours value
        Random random = new Random();
        total += noOfhours * random.nextDouble();

        for (Expense e: expenses)
        {
            total += e.getValue();
        }
        return total;
    }




}
