package at.hakwt.swp4.cohesion.payroll;

public class PayrollData
{
private Employee employee;
private Expense[] expenses;
private int noOfoverhours;

    public PayrollData(Employee employee, Expense[] expenses, int noOfoverhours)
    {
        this.employee = employee;
        this.expenses = expenses;
        this.noOfoverhours = noOfoverhours;
    }

    public Employee getEmployee()
    {
        return employee;
    }

    public Expense[] getExpenses()
    {
        return expenses;
    }

    public int getNoOfoverhours()
    {
        return noOfoverhours;
    }
}
