package at.hakwt.swp4.cohesion.payroll;

public class PayrollMain
{
    public static void main(String[] args)
    {
        Employee ferdi = new Employee(2000,"ferdi");
        Expense diaeten = new Expense(26.9);
        Expense kmGeld = new Expense( 150 * 0.42);
        Expense [] expenses = new Expense[] {diaeten, kmGeld};


        int noOfhours = 5;



        PayrolCalculator calculator = new PayrolCalculator();
        double amount = calculator.calculatePayment(ferdi, noOfhours, expenses);
        System.out.println(amount);
        calculator.print(ferdi, amount,"November 2022");

        DataImport import = new DataImport();
        PayrollData data = new import.DataImport("DataImport");
        calculator.calculatePayment(data.getEmployee(), data.getNoOfoverhours(), data.getExpenses());

        PayrollData data = calculator.importData("data.csv");
        calculator.calculatePayment(data.getEmployee(), data.getNoOfoverhours(), data.getExpenses());

        DataPrint xData = new DataPrint();
        xData.print(data.getEmployee(),amount,"November 2020");


        // ToDo Betrag runden
        // ToDo Verantworlichkeit "importieren" heraus ziehen (importData in eigene Klasse)
        // ToDo Verantworlichkeit "Ausgabe" herausziehen
        // ToDo "Übertsundenberechnung" in neue Klasse
    }
}
